package com.egci428.a21262

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.text.Html
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),SensorEventListener {
    private var sensorManager: SensorManager? = null
    private var lastUpdate: Long = 0
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        //To change text & Color in menu bar
        supportActionBar?.setTitle(
            Html.fromHtml("<font color='#000000'>" +
                    "\t".repeat(13) +
                    "Select Lucky Number</font>"))
        imageView.setImageResource(R.drawable.refresh)
        
        
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        lastUpdate = System.currentTimeMillis()
        //textView.text = kotlin.random.Random.nextInt(0, 10).toString()
        
        button.setOnClickListener {
        //To check whether user shaked his or her mobile
            if(luckNoTxt.text.toString()==""){
                Toast.makeText(this,"Please shake your mobile before\n" +
                        "\t".repeat(8) +
                        "proceeding",Toast.LENGTH_SHORT).show()
            }
            
            else {
                var intent = Intent(this, MainActivity2::class.java)
                intent.putExtra("RANDOM", luckNoTxt.text.toString())
                startActivity(intent)
                luckNoTxt.text = ""
            }
        }

    }
    
    //checking & collecting sensor data 
    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            getAccelerometer(event)
        }

    }
    //performing operation if user shake his/her mobile
    private fun getAccelerometer(event: SensorEvent) {
        val values = event.values
        val x = values[0]
        val y = values[1]
        val z = values[2]

        val accel = (x * x + y * y + z * z) / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH)
        val actualTime = System.currentTimeMillis()
        if (accel > 2) {
            if (actualTime - lastUpdate < 100) {
                return
            } else {
                lastUpdate = actualTime
                luckNoTxt.text = kotlin.random.Random.nextInt(20, 40).toString()
            }
        }
    }


    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }


    override fun onPause() {
        super.onPause()
        sensorManager!!.unregisterListener(this)
    }

    override fun onResume() {
        super.onResume()
        sensorManager!!.registerListener(this, sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL)
    }

}