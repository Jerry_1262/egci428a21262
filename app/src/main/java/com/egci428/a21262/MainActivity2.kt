package com.egci428.a21262

import android.os.Bundle
import android.text.Html
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main2.*

//this class to create random list and compare whether user win/loose
class MainActivity2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_main2)
        
        //to change title & and font 
        supportActionBar?.setTitle(
            Html.fromHtml("<font color='#000000'>" +
                    "\t".repeat(7) +
                    "Lucky Draw Result</font>"))
        
        //intialise intial value in array to create list of 5 random number
        var arrNo = arrayOf<String>("0","0","0","0","0")
        var k:String
        
        //this loops generate 5 random number and make sure that all 5 random numbers are different
        for (i in 0..4){
            arrNo[i]=kotlin.random.Random.nextInt(20, 40).toString()
            k=arrNo[i]
            while(k in arrNo){
                k=kotlin.random.Random.nextInt(20, 40).toString()
            }
            arrNo[i]=k
        }

        //this is to print all number to user
        numTxt.text=arrNo[0]+", "+arrNo[1]+", "+arrNo[2]+", "+arrNo[3]+", "+arrNo[4]
        //gets a random number generated on main page and check whether user win or loose
        val bundle= intent.extras
        var result:String
        if (bundle !=null){
            result=bundle.getString("RANDOM").toString()
            textView6.text=result
            if(result in arrNo){
                imageView2.setImageResource(R.drawable.star)
                resultTxt.text="You Win!!!"
            }
            else{
                imageView2.setImageResource(R.drawable.inbox)
                resultTxt.text="You Lose!!!"
            }

        }
        newDraw.setOnClickListener {
            finish()
        }

    }
    //this function is go back to mainpage if user press back button
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        if (id == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}